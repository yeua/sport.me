<?php

namespace Acme\SportBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Objectsinsports
 *
 * @ORM\Table(name="objectsInSports", indexes={@ORM\Index(name="idSport", columns={"idSport"})})
 * @ORM\Entity
 */
class Objectsinsports
{
    /**
     * @var integer
     *
     * @ORM\Column(name="idObject", type="integer", nullable=false)
     */
    private $idobject;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \Acme\SportBundle\Entity\Objects
     *
     * @ORM\ManyToOne(targetEntity="Acme\SportBundle\Entity\Objects")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idSport", referencedColumnName="id")
     * })
     */
    private $idsport;



    /**
     * Set idobject
     *
     * @param integer $idobject
     * @return Objectsinsports
     */
    public function setIdobject($idobject)
    {
        $this->idobject = $idobject;

        return $this;
    }

    /**
     * Get idobject
     *
     * @return integer 
     */
    public function getIdobject()
    {
        return $this->idobject;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idsport
     *
     * @param \Acme\SportBundle\Entity\Objects $idsport
     * @return Objectsinsports
     */
    public function setIdsport(\Acme\SportBundle\Entity\Objects $idsport = null)
    {
        $this->idsport = $idsport;

        return $this;
    }

    /**
     * Get idsport
     *
     * @return \Acme\SportBundle\Entity\Objects 
     */
    public function getIdsport()
    {
        return $this->idsport;
    }
}
