<?php

namespace Acme\SportBundle\Services;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Query\ResultSetMapping;

class SportEvents {

    protected $em;

    public function __construct( EntityManager $entityManager ) {
        $this->em = $entityManager;
    }

    /**
     * Get list of actual events
     * @TODO: correct query to get not all, but actual events
     * @TODO: add pagination
     * @return array
     */
    public function getEvents() {
        $rsm = new ResultSetMapping();
        $events = $this->em
                ->createQuery( "SELECT e, s.url sportUrl, s.title sportTitle
                FROM AcmeSportBundle:Events e
                LEFT JOIN AcmeSportBundle:Sports s WITH e.idsport = s.id", $rsm)
                ->getScalarResult();
        return $events;
    }

    /**
     * Get list of # last events
     * @return array
     */
    public function getLastEvents() {
        $rsm = new ResultSetMapping();
        $events = $this->em
                ->createQuery( "SELECT e, s.url sportUrl, s.title sportTitle
                FROM AcmeSportBundle:Events e
                LEFT JOIN AcmeSportBundle:Sports s WITH e.idsport = s.id
                ORDER BY e.id DESC", $rsm)
  			    ->setFirstResult(0)
				->setMaxResults(6)
                ->getScalarResult();
        return $events;
    }

    /**
     * Get single event data by its ID
     * @param type $id_event event #
     * @return array
     */
    public function getEventById($id_event) {
        $rsm = new ResultSetMapping();
        $events = $this->em
                ->createQuery( "SELECT e, s.url sportUrl, s.title sportTitle
                FROM AcmeSportBundle:Events e
                LEFT JOIN AcmeSportBundle:Sports s WITH e.idsport = s.id
                        WHERE e.id = ?1", $rsm)
                ->setParameter(1, $id_event)
                ->getScalarResult();
        return $events;
    }

}