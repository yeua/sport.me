<?php

namespace Acme\SportBundle\Services;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Query\ResultSetMapping;

class SportSports {

    protected $em;

    public function __construct( EntityManager $entityManager ) {
        $this->em = $entityManager;
    }

    /**
     * Get list of actual sports
     * @TODO: add pagination
     * @return array
     */
    public function getSports() {
        $rsm = new ResultSetMapping();
        $sports = $this->em
                ->createQuery( "SELECT s
                FROM AcmeSportBundle:Sports s", $rsm)
                ->getScalarResult();
        return $sports;
    }

    /**
     * Get single sport data by its url
     * @param type $url
     * @return array
     */
    public function getSportsByUrl($url) {
        // $rsm = new ResultSetMapping();
        // $sport = $this->em
        //         ->createQuery( "SELECT s
        //         FROM AcmeSportBundle:Sports s
        //         WHERE s.url = ?1", $rsm)
        //         ->setParameter(1, $url)
        //         ->getScalarResult();
        // return $sport;

        switch ($url) {
            case 'football':
                
                $a = array(
                    array(
                        'id' => 1, 
                        'title' => 'Champion Kids', 
                        'desc' => 'Детский футбольный клуб для детей от 3 до 7 лет. Программа чемпиона. Регулярный контроль достижений. Безопасный инвентарь. Футбольные сборы, турниры и лагеря. Трансляция тренировок на экране. Уютный фреш-бар', 
                        'url' => 'champion-kids', 
                        'logo' => 'champkids.png', 
                        'address' => 'г.Киев ул. Кудри 5, м. Лыбедская', 
                        'rate' => '5', 
                        'lat' => '', 
                        'long' => '', 
                    ),
                    array(
                        'id' => 2, 
                        'title' => 'Дворовая лига', 
                        'desc' => 'Главный чемпионат Ассоциации дворового футбола. Соревнование, которое несет миссию всей Ассоциации дворового футбола  и направленно на самоорганизацию людей, которые не знают где и с кем поиграть, в организованную структуру - команду.', 
                        'url' => 'adf', 
                        'logo' => 'dvorligapngfoto150.png', 
                        'address' => '', 
                        'rate' => '4', 
                        'lat' => '', 
                        'long' => '', 
                    ),
                    array(
                        'id' => 3, 
                        'title' => 'ДЮФШ «Динамо» Киев им. В.В. Лобановского', 
                        'desc' => 'Обучение в ДЮФШ «Динамо» имени Валерия Лобановского проходят ребята с 6 до 17 лет. Ежегодно проводится конкурсный набор новичков и дополнительный конкурсный набор во все возрастные группы. Проходят они в мае и сентябре по субботам и воскресеньям с 10.00 ч. по адресу: ул.Салютная, 35.', 
                        'url' => 'dynamo', 
                        'logo' => 'dynamo.png', 
                        'address' => 'г.Киев ул.Салютная, 35.', 
                        'rate' => '4.5', 
                        'lat' => '', 
                        'long' => '', 
                    ),
                );

                break;
            
            default:
                $a = null;
                break;
        }
        return $a;
    }

}