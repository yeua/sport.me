<?php

namespace Acme\SportBundle\Services;

class Helpers {

    public function checkForFiles($path) {
        $f = null;
        $files = scandir( $path );
        foreach ($files as $key => $value) {
            if($value == '.' || $value == '..') {
                continue;
            }
            $f[] = $value;
        }
        return $f;
    }

    public function generateRandomUserPics($num = 10) {
        $a = array(
            array('men', 99), 
            array('women', 95), 
            array('lego', 9 )
        );
        for($i = 1; $i <= $num; $i++) {
            $u = $a[rand(0,2)];
            $up[] = 'https://randomuser.me/api/portraits/thumb/'.$u[0].'/'.rand(1,$u[1]).'.jpg';
        }
        return $up;
    }

}