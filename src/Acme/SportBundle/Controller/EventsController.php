<?php

namespace Acme\SportBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class EventsController extends Controller
{
    public function indexAction()
    {
    	$events = $this->get('sport_events')->getEvents();
        return $this->render('AcmeSportBundle:Default:events.html.twig', array('events' => $events, ));
    }

    public function eventAction($id) {
        $event = $this->get('sport_events')->getEventById($id);
        $e = $event[0];
        $path = $_SERVER['DOCUMENT_ROOT'] . '/images/events/' . $e['e_id'];
        $helpers = $this->get('helpers');
        $p = $helpers->checkForFiles($path);
        $u = $helpers->generateRandomUserPics( rand(10,15) );
        return $this->render('AcmeSportBundle:Default:event.html.twig', array('event' => $e, 'photos' => $p, 'users' => $u));
    }
}
