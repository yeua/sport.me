<?php

namespace Acme\SportBundle\Controller;

use Acme\SportBundle\Entity\Events;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        $events = $this->get('sport_events')->getLastEvents();

        foreach ($events as $key => $value) {
	        $path = $_SERVER['DOCUMENT_ROOT'] . '/images/events/' . $value['e_id'];
	        $helpers = $this->get('helpers');
	        $p = $helpers->checkForFiles($path);
	        $events[$key]['photo'] = $p[0];
        }

        $sports = $this->get('sport_sports')->getSports();
        return $this->render('AcmeSportBundle:Default:index.html.twig', array('events' => $events, 'sports' => $sports));
    }

    public function sportsAction()
    {
		$sports = $this->get('sport_sports')->getSports();
        return $this->render('AcmeSportBundle:Default:sports.html.twig', array(
        		'sports' => $sports
        	)
        );
    }

    public function sportAction($url)
    {
		$sports = $this->get('sport_sports')->getSportsByUrl($url);
        return $this->render('AcmeSportBundle:Default:sport.html.twig', array(
        		'sports' => $sports
        	)
        );
    }
}
