<?php

namespace Acme\SportBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class HandbookController extends Controller
{
    public function indexAction()
    {
        return $this->render('AcmeSportBundle:Default:handbook.html.twig');
    }
}
