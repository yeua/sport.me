<?php

namespace Acme\SportBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class QuestionsController extends Controller
{
    public function indexAction()
    {
        return $this->render('AcmeSportBundle:Default:questions.html.twig');
    }

    public function questionAction($id)
    {
        return $this->render('AcmeSportBundle:Default:question.html.twig');
    }
}
