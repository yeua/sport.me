-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Feb 22, 2015 at 03:45 AM
-- Server version: 5.5.41-0ubuntu0.14.04.1
-- PHP Version: 5.5.9-1ubuntu4.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `symfony`
--

-- --------------------------------------------------------

--
-- Table structure for table `events`
--

CREATE TABLE IF NOT EXISTS `events` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idSport` int(11) NOT NULL,
  `date_start` datetime NOT NULL,
  `date_end` datetime NOT NULL,
  `title` varchar(255) CHARACTER SET utf8 NOT NULL,
  `desc` text CHARACTER SET utf8 NOT NULL,
  `logo` varchar(255) CHARACTER SET utf8 NOT NULL,
  `address` text CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idSport` (`idSport`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `events`
--

INSERT INTO `events` (`id`, `idSport`, `date_start`, `date_end`, `title`, `desc`, `logo`, `address`) VALUES
(1, 1, '2015-02-11 00:00:00', '2015-02-28 00:00:00', 'Lorem ipsum', 'Lorem Ipsum - це текст-"риба", що використовується в друкарстві та дизайні. Lorem Ipsum є, фактично, стандартною "рибою" аж з XVI сторіччя, коли невідомий друкар взяв шрифтову гранку та склав на ній підбірку зразків шрифтів. "Риба" не тільки успішно пережила п''ять століть, але й прижилася в електронному верстуванні, залишаючись по суті незмінною. Вона популяризувалась в 60-их роках минулого сторіччя завдяки виданню зразків шрифтів Letraset, які містили уривки з Lorem Ipsum, і вдруге - нещодавно завдяки програмам комп''ютерного верстування на кшталт Aldus Pagemaker, які використовували різні версії Lorem Ipsum.', '', 'На відміну від поширеної думки Lorem Ipsum не є випадковим набором літер. Він походить з уривку класичної латинської літератури 45 року до н.е'),
(2, 2, '2015-02-18 00:00:00', '2015-02-27 00:00:00', 'Boulevard Of Broken Dreams', 'Вже давно відомо, що читабельний зміст буде заважати зосередитись людині, яка оцінює композицію сторінки. Сенс використання Lorem Ipsum полягає в тому, що цей текст має більш-менш нормальне розподілення літер на відміну від, наприклад, "Тут іде текст. Тут іде текст." Це робить текст схожим на оповідний. Багато програм верстування та веб-дизайну використовують Lorem Ipsum як зразок і пошук за терміном "lorem ipsum" відкриє багато веб-сайтів, які знаходяться ще в зародковому стані. Різні версії Lorem Ipsum з''явились за минулі роки, деякі випадково, деякі було створено зумисно (зокрема, жартівливі).', '', '200 слів латини та цілий набір моделей речень - це дозволяє генерувати Lorem Ipsum, який виглядає осмислено. Таким чином, згенерований Lorem Ipsum не міститиме повторів, жартів, нехарактерних для латини слів і т.ін.\n	\n	абзаців\n	слів\n	байт\n	списків\n	\nПочати з ''Lorem ipsum dolor sit amet...''\n ');

-- --------------------------------------------------------

--
-- Table structure for table `objects`
--

CREATE TABLE IF NOT EXISTS `objects` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `desc` text NOT NULL,
  `url` varchar(255) NOT NULL,
  `logo` varchar(255) NOT NULL,
  `lat` varchar(255) NOT NULL,
  `long` varchar(255) NOT NULL,
  `address` text NOT NULL,
  `rate` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `title` (`title`),
  KEY `rate` (`rate`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `objectsInSports`
--

CREATE TABLE IF NOT EXISTS `objectsInSports` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idSport` int(11) NOT NULL,
  `idObject` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idSport` (`idSport`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `sports`
--

CREATE TABLE IF NOT EXISTS `sports` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `url` varchar(255) CHARACTER SET utf8 NOT NULL,
  `title` varchar(255) CHARACTER SET utf8 NOT NULL,
  `description` text CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`id`),
  KEY `title` (`title`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `sports`
--

INSERT INTO `sports` (`id`, `url`, `title`, `description`) VALUES
(1, 'football', 'футбол', '<p>Футбо́л асоціації, скорочено просто футбол (від англ. football, association football), інші назви: ко́паний м''яч або ко́панка, сокер (англ. soccer) — один з різновидів футболу, олімпійський вид спорту, командна спортивна гра, у якій беруть участь дві команди по одинадцять гравців у кожній.\n</p><p>\nФутбол є одним із найпопулярніших видів спорту у світі. У футбол грають м''ячем на прямокутному трав''яному полі (іноді траву замінюють штучним покриттям) з воротами на двох протилежних сторонах поля. Метою гри є забити якомога більше голів; тобто, завести м''яч у ворота супротивника якомога більше разів. Головною особливістю футболу є те, що гравці (за винятком воротарів) не можуть в межах ігрового поля торкатися м''яча руками. Переможцем гри є команда, що забила за час матчу (два тайми по 45 хвилин) більше голів, ніж супротивник.\n</p><p>\nНайпрестижнішим турніром з футболу є Чемпіонат світу з футболу.</p>'),
(2, 'basketball', 'баскетбол', '<p>Баскетбо́л (від англ. basket — «кошик» і англ. ball — «м''яч», інші назви: кошикі́вка[1] або коші́вка[2]) — спортивна командна гра з м''ячем, який закидують руками в кільце із сіткою (кошик), закріпленою на щиті на висоті 3 метри 5 сантиметрів (10 футів) над майданчиком.\n</p><p>\nГрають дві команди по 5 осіб на майданчику 28 х 15 м. На коротких сторонах майданчика укріплені на щитах на висоті 305 см «кошики» — металеві кільця діаметром 45 см з мотузяною сіткою без дна. Переможцем визнається команда, якій вдалось більше число разів закинути м''яч у «кошик» супротивників. Тривалість гри — чотири періоди по 10 хвилин, з двохвилинними перервами між першою та другою і третьою й четвертою чвертями, та великою перервою у 15 хвилин між другим та третім періодом. В Національній баскетбольній Асоціації гра триває 48 хвилин і розбивається на 4 чверті.\n</p><p>\nПерший чоловічий чемпіонат світу проведено в 1950-у році, а в 1953-му — жіночий. Зараз проводяться кожні чотири роки. Найпрестижніші змагання серед збірних команд країн — Олімпійські ігри, Чемпіонат світу з баскетболу, а серед клубних — чемпіонат НБА та в Європі — Євроліга УЛЕБ, Єврокубок та Кубок виклику ФІБА.</p>'),
(3, 'hockey', 'хокей', '<p>Хоке́й (гаківка; раніше за Харківським правописом: го́кей) — спільна назва для командних спортивних ігор із ключкою, мета яких закинути м''яч або шайбу в ворота протилежної команди.\r\n</p><p>\r\nРозрізняють: Хокей на траві; Хокей із шайбою (Інша назва канадський хокей); Хокей з м''ячем, який називають також російським хокеєм або бенді.</p>\r\n<p>\r\nПодібна до хокею й старовинна українська гра ковіньки (український відповідник російського слова "клюшка").<p>');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `events`
--
ALTER TABLE `events`
  ADD CONSTRAINT `events_ibfk_1` FOREIGN KEY (`idSport`) REFERENCES `sports` (`id`);

--
-- Constraints for table `objectsInSports`
--
ALTER TABLE `objectsInSports`
  ADD CONSTRAINT `objectsInSports_ibfk_1` FOREIGN KEY (`idSport`) REFERENCES `objects` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
