$(document).ready(function () {
    $(".right_arrow").click(function () {
        $('.slider-items').animate({left: '-222px'}, 200, function () {
            $(".slider-items .one_event-block").eq(0).clone().appendTo(".slider-items");
            $(".slider-items .one_event-block").eq(0).remove();
            $(".slider-items").css({"left": "0px"});
        });
    });

    $(".left_arrow").click(function () {
        $(".slider-items .one_event-block").eq(-1).clone().prependTo(".slider-items");
        $(".slider-items").css({"left": "-222px"});
        $(".slider-items .one_event-block").eq(-1).remove();
        $(".slider-items").animate({left: "0px"}, 200);
    });
});