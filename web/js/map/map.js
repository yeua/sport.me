(function (window, emr, L, undefined) {
    var StorageTileLayer = L.TileLayer.extend({
        _imageToDataUri: function (image) {
            var canvas = window.document.createElement('canvas');
            canvas.width = image.width;
            canvas.height = image.height;

            var context = canvas.getContext('2d');
            context.drawImage(image, 0, 0);

            return canvas.toDataURL('image/png');
        },
        _tileOnLoadWithCache: function () {
            var storage = this._layer.options.storage;
            if (storage) {
                storage.add(this._storageKey, this._layer._imageToDataUri(this));
            }
            L.TileLayer.prototype._tileOnLoad.apply(this, arguments);
        },
        _setUpTile: function (tile, key, value, cache) {
            tile._layer = this;
            if (cache) {
                tile._storageKey = key;
                tile.onload = this._tileOnLoadWithCache;
                tile.crossOrigin = 'Anonymous';
            } else {
                tile.onload = this._tileOnLoad;
            }
            tile.onerror = this._tileOnError;
            tile.src = value;
        },
        _loadTile: function (tile, tilePoint) {
            this._adjustTilePoint(tilePoint);
            var key = tilePoint.z + ',' + tilePoint.y + ',' + tilePoint.x;

            var self = this;
            if (this.options.storage) {
                this.options.storage.get(key, function (value) {
                    if (value) {
                        self._setUpTile(tile, key, value, false);
                    } else {
                        self._setUpTile(tile, key, self.getTileUrl(tilePoint), true);
                    }
                }, function () {
                    self._setUpTile(tile, key, self.getTileUrl(tilePoint), true);
                });
            } else {
                self._setUpTile(tile, key, self.getTileUrl(tilePoint), false);
            }
        }
    });

    emr.on('mapLoad', function (storage) {

        var sports_facilities = [
            [50.44884, 30.52134, 'swimming', 'Бассейн "Юность"', "./images/obj/1.jpg", "<p>Украина, г. Киев, ул. Бастионная, 7</p><p>ванна для плаванния, размер 50х25 м, глубина от 1,20 м до 5,5 м</p>"],
            [50.44575, 30.52834, 'swimming', 'Бассейн "Спартак"', "./images/obj/2.jpg", "<p>Украина, г. Киев, ул. Фрунзе, 105</p><p>Работает ежедневно с 8:00 до 21:00.</p>"],
            [50.45233, 30.52645, 'football', 'Футбольний майданчик', "./images/obj/1.jpg", "<p>Украина, г. Киев, ул. В. Лобановского.</p>"],
            [50.45018, 30.52722, 'gym', 'Фитнес-клуб «Олимп»', "./images/obj/1.jpg", "<p>Украина, г. Киев, ул. Фрунзе, 105</p><p>Работает ежедневно с 8:00 до 21:00.</p>"],
            [50.43518, 30.53722, 'gym', 'Фитнес-клуб «Баобаб»', "./images/obj/1.jpg", "<p>Украина, г. Киев, ул. Фрунзе, 105</p><p>Работает ежедневно с 8:00 до 21:00.</p>"],
            [50.41852, 30.47161, 'basketball', 'Баскетбольний майданчик', "./images/obj/3.jpg", "<p>Украина, г. Киев, ул. Фрунзе, 105</p><p>Работает круглосоточно</p>"],
            [50.41330, 30.47736, 'basketball', 'Баскетбольний майданчик', "./images/obj/4.jpg", "<p>Украина, г. Киев, ул. Фрунзе, 105</p><p>Работает круглосоточно</p>"]
        ];

        var map = L.map('map');
        var dafault_place = [49.422981, 26.987133];
        var Minsk = [53.902254, 27.561850];

        var swimming = new L.LayerGroup();
        var gym = new L.LayerGroup();
        var football = new L.LayerGroup();
        var basketball = new L.LayerGroup();

        var markers = new L.MarkerClusterGroup();
        var searchControl = new L.esri.Geocoding.Controls.Geosearch().addTo(map);

        var results = new L.LayerGroup().addTo(map);
        searchControl.on('results', function (data) {
            results.clearLayers();
            for (var i = data.results.length - 1; i >= 0; i--) {
                results.addLayer(L.marker(data.results[i].latlng));
            }
        });

        function onLocationFound(e) {
            var radius = e.accuracy / 2;

            L.marker(e.latlng).addTo(map)
                    .bindPopup("Ви знаходитесь в радіусі " + radius + " метрів від даної точки").openPopup();

            L.circle(e.latlng, radius).addTo(map);
        }

        function onLocationError(e) {
            alert(e.message);
        }

        map.on('locationfound', onLocationFound);
        map.on('locationerror', onLocationError);
        markers.on('clusterclick', function (a) {
            map.addLayer(new L.Polygon(a.layer.getConvexHull()));
        });

        if (map.locate()) {
            map.locate({setView: true, zoom: 20, layers: [swimming, gym, football, basketball]});
        }
        else {
            map.setView(dafault_place, 13);
        }
        new StorageTileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {maxZoom: 25, minZoom: 4, storage: storage}).addTo(map);
        emr.fire('mapLoaded');

        /*map.on('click', function(e) {
         alert("Lat, Lon : " + e.latlng.lat + ", " + e.latlng.lng)
         });*/
        var LeafIcon = L.Icon.extend({
            options: {
                iconSize: [32, 32],
                iconAnchor: [22, 94],
                popupAnchor: [-3, -96]
            }
        });

        var swimmingIcon = new LeafIcon({iconUrl: 'http://sport.me/images/categorys_sport/swimming.png'}),
                footballIcon = new LeafIcon({iconUrl: 'http://sport.me/images/categorys_sport/football.png'}),
                gymIcon = new LeafIcon({iconUrl: 'http://sport.me/images/categorys_sport/biceps.png'}),
                basketballIcon = new LeafIcon({iconUrl: 'http://sport.me/images/categorys_sport/basketball.png'});

        for (var i = 0; i < sports_facilities.length; i++) {
            var rows = sports_facilities[i];
            var description = "<div class='obj-desc'><div class='table-cell'><img class='obj' src='" + rows['4'] + "'/></div><div class='table-cell'><div class='title'>" + rows['3'] + "</div><div class='desc'>" + rows['5'] + "</div></div><div class='clearfix'></div></div>";
            switch (rows['2']) {
                case "swimming":
                    var Marker = L.marker([rows['0'], rows['1']], {icon: swimmingIcon}).bindPopup(description).addTo(swimming);
                    break;
                case "football":
                    var Marker = L.marker([rows['0'], rows['1']], {icon: footballIcon}).bindPopup(description).addTo(football);
                    break;
                case "gym":
                    var Marker = L.marker([rows['0'], rows['1']], {icon: gymIcon}).bindPopup(description).addTo(gym);
                    break;
                case "basketball":
                    var Marker = L.marker([rows['0'], rows['1']], {icon: basketballIcon}).bindPopup(description).addTo(basketball);
                    break;
            }

            markers.addLayer(Marker);
        }

        var overlays = {
            "Басейни": swimming,
            "Тренажерні зали": gym,
            "Футбольні майданчики": football,
            "Баскетбольні майданчики": basketball
        };

        //map.addLayer(swimming);
        //map.addLayer(gym);
        //map.addLayer(football);
        //map.addLayer(basketball);

        map.addLayer(markers);

        $('.category_sport').click(function () {
            //map.locate({setView: true, zoom: 6});
            //map.addLayer(markers);
            map.removeLayer(markers);
            var id = $(this).attr('id');
            switch (id) {
                case 'swimming':
                    map.addLayer(swimming);
                    break;
                case 'gym':
                    map.addLayer(gym);
                    break;
                case 'football':
                    map.addLayer(football);
                    break;
                case 'basketball':
                    map.addLayer(basketball);
                    break;
            }
        });
    });
})(window, window.offlineMaps.eventManager, L);